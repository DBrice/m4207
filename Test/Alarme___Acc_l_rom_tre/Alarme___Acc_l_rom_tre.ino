#include <LGPS.h>
//Librairie Acceleromètre//
#include <Wire.h>
#include "MMA7660.h"
MMA7660 accelemeter;
//                      //

//Définition des LEDs//
const int ledR = 5;
const int ledB = 6;
//                  //

//Définition du capteur de toucher//
const int touch = 7;
//        //        //           //

//Déclarations des variables de l'accéleromètre//
float ax,ay,az;
//        //        //          //

//Déclarations variable//
boolean test = true;
//  //  //  //  //  //  //

//Déclaration des variables de temps//
unsigned long previousMillis=0 ;
unsigned long interval = 300000L;
//  //  //  //  //  //  //  //  //  //

//Déclaration Alarme activée//
boolean isOn = false;
int onTime = 0;
int cptOn = 0;


//Déclaration pour SMS//
#include <LGSM.h>
#include <LTask.h>

String remoteNumber;
String number = "0692833436";
String sms_text;
String sms_text2;
boolean call = false;
char charbuffer[20];
//  //  //  //  //  //  //

//Déclaration GPS//
gpsSentenceInfoStruct info;
char buff[256];
//  //  //  //  //  //  //  //

////////////////////////////////////////////
//INITIALISATION GPS//////////////////////////
////////////////////////////////////////////

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr){
double latitude;
double longitude;
int tmp, hour, minute, second, num ;
    
  tmp = getComma(1, GPGGAstr);
  hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
  minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
  second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
  tmp = getComma(2, GPGGAstr);
  latitude = getDoubleNumber(&GPGGAstr[tmp]);
  tmp = getComma(4, GPGGAstr);
  longitude = getDoubleNumber(&GPGGAstr[tmp]);
  sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude, "UTC timer %2d-%2d-%2d", hour, minute, second);

  sms_text2 = String(hour);
  sms_text2 += ":";
  sms_text2 += String(minute);
  sms_text2 += ":";
  sms_text2 += String(second);
  sms_text2 += " \n";
  sms_text2 += "Latitude en DMS : ";
  sms_text2 += String(latitude);
  sms_text2 += " \n";
  sms_text2 += "Longitude en DMS : ";
  sms_text2 += String(longitude);
  sms_text2 += "\n";
  sms_text2 += "Envoyez on10 pour arreter les alertes";
  
}
////////////////////////////////////////////////////////////////
//Fonction acceleromètre//
void accelerometre() {
  accelemeter.getAcceleration(&ax,&ay,&az);
}
//  //  //  //  //  //  //  //  //  //  //  //

//Fonction permet d'enclencher l'alarme aprés toucher ou acceleromètre//
void alarme() {
  int toucher = digitalRead(touch);
  if (ax > 1 || ay < -1 || az > 2 || toucher == HIGH){
  for (int i = 0; i < 20; i ++){
    allumerLed();
    }
  for (int i = 0; i < 3; i ++){
    SmS();
  }
   isOn = true; 
  }
  else {
    digitalWrite(ledR, LOW);
    digitalWrite(ledB, LOW);
  }
}
///////Fonction allumage LED pour alarme/////////
void allumerLed(){ 
    digitalWrite(ledR, test);
    delay(200);
    test = !test;
    digitalWrite(ledB, test);
    delay(200);
}   
//  //  //  //  //  //  //  //  //  //  //  //

void SmS(){
//********************************   SMS    *****************************
          number.toCharArray(charbuffer, 20);
          LSMS.beginSMS(charbuffer);
          LSMS.print(sms_text);
          LSMS.print(sms_text2);
          Serial.println("To: " + number);
          Serial.println("Text: " + sms_text);
          Serial.println("Text: " + sms_text2);
          sms_text = "Voiture attaquee ";
          LSMS.endSMS();
      }
void infoGPS(){
    LGPS.getData(&info); 
    parseGPGGA((const char*)info.GPGGA);
}

void AlerteGPS() {
  if( millis() - previousMillis >= interval) {
    previousMillis = millis();   
      number.toCharArray(charbuffer, 20);
      LSMS.beginSMS(charbuffer);
      LSMS.print(sms_text2);
      Serial.println("To: " + number);
      Serial.println("Text: " + sms_text2);
      LSMS.endSMS();
    }
}


void setup() {
  //Initialisation de l'acceléromètre//
  accelemeter.init(); 
  //            //          //      //

  //Initialisation des LEDs//
  pinMode(ledR, OUTPUT);
  pinMode(ledB, OUTPUT);
  //      //          //   //

  //Initialistion du capteur de toucher//
  pinMode(touch, INPUT);
  //      //    //    //    //  //    //
  Serial.begin(115200);

  //Initialisation du GPS//
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(100);
  //  //  //  //  //  //  //  //  //  //  //  //  //

  while(!LSMS.ready())
    {
        delay(50);
    }
    
    Serial.println("GSM OK!!");
  
}


void loop() {

char p_num[20];
    int len = 0;
    char dtaget[500];

    if(LSMS.available()) // Check if there is new SMS
    {
    
        LSMS.remoteNumber(p_num, 20); // display Number part
        Serial.println("There is new message.");
        
        Serial.print("Number:");
        Serial.println(p_num);
        Serial.print("Content:"); // display Content part     

        while(true)
        {
            int v = LSMS.read();
            if(v < 0)
            break;

            dtaget[len++] = (char)v;
            Serial.print((char)v);
        }

        Serial.println();
        LSMS.flush(); // delete message

        
        if((dtaget[0] == 'O' && dtaget[1] == 'N') || (dtaget[0] == 'o' && dtaget[1] == 'n'))
        {
            onTime = 0;
            
            for(int i=2; i<len; i++)
            {
                if((dtaget[i]>='0') && (dtaget[i]<='9'))
                {
                    onTime = 10*(i-2) + (dtaget[i]-'0');
                }
                else
                {
                    break;
                }
            }
        }
        else if((dtaget[0] == 'O' && dtaget[1] == 'F' && dtaget[2] == 'F') || (dtaget[0] == 'o' && dtaget[1] == 'f' && dtaget[2] == 'f'))
        {
            onTime = 0;
        }
    }
    
    if(onTime>0)
    {
        cptOn = 0;
        isOn = false;
        onTime--;
    }
    else
    {

  infoGPS();
  accelerometre();
  alarme();

  /////////////////Code GPS//////////////////////////////////
  if (isOn == true ){
    cptOn++;
    if (cptOn > 0){
      AlerteGPS();
    }
  }
    }
  
  
  
} 
  

  
