#include <LiquidCrystal.h>
#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;
//Sa concerne le bouton et le buzzer (qui est la led maintenant)
const int button = 2;       // connect a button
const int buzzer = 3;       // connect a buzzer
//////////////////////

//Changer la couleur de l'affichage//
const int colorR = 255;
const int colorG = 0;
const int colorB = 255;
//////////////////////////////////
//Sonde température//
#include <math.h>

const int B = 4275;               // B value of the thermistor
const int R0 = 100000;            // R0 = 100k
const int pinTempSensor = A0;     // Grove - Temperature Sensor connect to A0

#if defined(ARDUINO_ARCH_AVR)
#define debug  Serial
#elif defined(ARDUINO_ARCH_SAMD) ||  defined(ARDUINO_ARCH_SAM)
#define debug  SerialUSB
#else
#define debug  Serial
#endif
//////////////////////////////////////

//Code pour la sonde sonore//
const int pinAdc = A1;
////////////////////////////
void setup()
{
    pinMode(button, INPUT); //set button as an INPUT device
    pinMode(buzzer, OUTPUT);   //set LED as an OUTPUT device

   //SETUP ECRAN LCD//
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    
    lcd.setRGB(colorR, colorG, colorB);
    
    // Print a message to the LCD.
    lcd.print("Ouistiti"); //On peut écrire 15 caractère
    delay(1000);
    /////////////////////////////////////////////////////
 
    //Pour surveiller le niveau sonor et la température//
    Serial.begin(115200);
    ///////////////////////////////////
    
}
void loop()
{
    //C'est pour tester la LED (c'était un buzzer)
    int btn = digitalRead(button); //read the status of the button
    digitalWrite(buzzer, btn);
    delay(10);

    //Code pour l'écran LCD
    // set the cursor to column 0, line 1
    // (note: line 1 is the second row, since counting begins with 0):
    lcd.setCursor(0, 1);
    // print the number of seconds since reset:
    lcd.print("Dans la jungle");
    delay(100);

    //Code pour la sonde température//
    int temp = analogRead(pinTempSensor);

    float R = 1023.0/temp-1.0;
    R = R0*R;

    float temperature = 1.0/(log(R/R0)/B+1/298.15)-273.15; // convert to temperature via datasheet

    Serial.print("temperature = ");
    Serial.println(temperature);

    delay(100);
    ////////////////////////////////////

    //Code Sonde Sonore//
    long sum = 0;
    for(int i=0; i<32; i++)
    {
        sum += analogRead(pinAdc);
    }

    sum >>= 5;

    Serial.println(sum);
    delay(5);
    ////////////////////////////////////
}
